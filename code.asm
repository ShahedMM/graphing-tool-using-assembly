; multi-segment executable file template.

data segment
    a_input db "a - enter a number between -1 and 1: $"
    b_input db "b - enter a number between -3 and 3: $"
    c_input db "c - enter a number between -10 and 10: $" 
    root_1 db "first root: $"
    root_2 db "second root: $"
    no_root db "no real roots/no solution $"
    linear_sol db "solution of linear equation: " 
    x_pos db "x pos: $"
    y_pos db "y pos: $"
    a db 0 
    b db 0
    c dw 0
ends

stack segment
    dw   128  dup(0)
ends

code segment
start:
; set segment registers:
    mov ax, data
    mov ds, ax
    mov es, ax
    
    lea dx,a_input
    mov ah,9
    int 21h
    call readInput
    mov [a],bl    
    call printEnter
    lea dx,b_input
    mov ah,9
    int 21h
    call readInput
    mov [b],bl
    call printEnter
    lea dx,c_input
    mov ah,9
    int 21h
    call readInput
    mov [c], bx
    call printEnter

printRoot:    
    mov al,[a]
    cmp al,0
    je linear
    call discriminant ;in bx
    cmp bx,0
    jl no_valid_root
    call square_root ;in cx
    call root ;in ax and dx
    push dx
    push ax
    lea dx,root_1
    mov ah,9
    int 21h
    pop ax
    call printNum
    call printEnter
    lea dx,root_2
    mov ah,9
    int 21h
    pop ax
    call printNum
    jmp start_delay 
linear:  
    mov al,[b]
    cmp al,0
    je no_valid_root
    lea dx,linear_sol
    mov ah,9
    int 21h
    call sol
    call printNum 
    jmp start_delay

no_valid_root:
    lea dx,no_root
    mov ah,9
    int 21h
    
start_delay:    
    mov cx,100
delay:     
    mul bl
    loop delay
                 
start_draw: 
    call draw 
    
        
    mov ax,0
    int 33h 
    mov di,0  
    mov bx,0
   
   
readMouse: 
    push bx
    mov ah,2
    mov bh,0
    mov dl,0
    mov dh,0
    int 10h 
    
    mov ax,3
    int 33h 
    
    
    
   
    push dx
    lea dx,x_pos
    mov ah,9
    int 21h
    mov ax,cx
    shr ax,1
    sub ax,160
    ;mov [ans],ax
    call printNum
        
    mov cx,si
    cmp cx,di
    jg jump
    sub di,cx
    mov cx,di
      
   clean: 
    cmp cx,0
    je jump  
    mov dx,' '
    mov ah,02h 
    int 21h 
    loop clean
                  
    jump:
       mov di,si
    
    call printEnter
    
    lea dx,y_pos
    mov ah,9
    int 21h 
    pop ax
    sub ax,100
    neg ax
    ;mov [ans],ax
    call printNum
    
    pop bx    
    mov cx,si
    cmp cx,bx
    jg jump2
    sub bx,cx
    mov cx,bx
      
   clean2: 
    cmp cx,0
    je jump2  
    mov dx,' '
    mov ah,02h 
    int 21h 
    loop clean2
                  
    jump2:
       mov bx,si
       
    call printEnter
             
    
       
       
    jmp readMouse                        ;
                    

   
endprog:    
    mov ax, 4c00h ; exit to operating system. 
    int 21h
   
plot:   
    mov bp,sp
    mov ax,[bp+2]
	mov cx,320
	mul cx 
	add ax,[bp+4]
	mov bx,ax
	mov ax,[bp+6]
	mov es:[bx],al
	ret          
         
draw: 
    mov al, 13h ;enter graphics mode
	mov ah, 0
	int 10h
	
	mov ax,0a000h
	mov es,ax   
 
    
    mov bx,0
VertLine:
    cmp bx,200 ;max y pixel
    ja initial
    push bx 
    mov ax,7   
    push ax
    mov ax,160
    push ax
    mov ax,bx
    push ax
    call plot
    add sp,6
    pop bx
    add bx,1
    jmp VertLine   
initial:
    mov bx,0
HorizLine:
    cmp bx,320 ;max x pixel
    ja startcalc
    push bx 
    mov ax,7   
    push ax
    mov ax,bx
    push ax
    mov ax,100
    push ax
    call plot
    add sp,6
    pop bx
    add bx,1
    jmp HorizLine


startcalc:   
    mov dx,0 ;try plotting x=[-9:9], x=dx
    
calcpos:     ;calculate y = ax^2+bx+c for x[0:9]
    cmp dl,10
    jz initial2
    mov al,[a]
    imul dl
    imul dl
    push ax
    mov al,[b]
    imul dl
    pop bx
    add ax,bx
    add ax,[c]
    
    push dx
    mov bx,7
    push bx
    mov bx,dx
    add bx,160 
    push bx 
    mov bx,100
    
    sub bx,ax
    cmp bx,200
    ja overflowpos
    cmp bx,0
    jb overflowpos 
    push bx 
    call plot
    add sp,6
    pop dx
    add dl,1
    jmp calcpos
    
overflowpos:
    add sp,6 
initial2:   
    mov dx,-1
    
calcneg:      ;calculate y = ax^2+bx+c for x[-9:-1]
    cmp dx,-10
    jz returnCalcFinal
    mov al,[a]
    imul dl
    imul dl
    push ax
    mov al,[b]
    imul dl
    pop bx
    add ax,bx
    add ax,[c]
    
    push dx
    mov bx,7
    push bx
    mov bx,160
    add bx,dx 
    push bx 
    mov bx,100
    sub bx,ax
    
    cmp bx,200
    ja overflowneg
    cmp bx,0
    jb overflowneg
    
    push bx 
    call plot
    add sp,6
    pop dx
    sub dl,1
    jmp calcneg
overflowneg:
    add sp,6  
    returnCalcFinal:
    ret    
    
readInput:
    mov dh,0 ; if(dh==1) number is negative 
    mov bx,0 ; input number is stored in bl
    mov ah,1
    int 21h
    cmp al,'-'
    jz negativeInput
    cmp al,'+' ;check if input char is '+' (if not then it's assumed to be a digit)
    jz loopInput     ; start reading number
    sub al,48  ; if not ('+' or '-') then add this digit to the number
    mov ah,0  
    mov bx,ax 
    jmp loopInput
negativeInput: 
    mov dh,1
    
    
loopInput:
     mov ah,1
    int 21h
    cmp al,13
    jz exitInput
    sub al,48
    mov dl,al
    mov al,10
    mul bl  ;bl*10
    add al,dl
    mov bx,ax
    loop loopInput
    
exitInput:
    cmp dh,0
    jz returnInput
    neg bx
returnInput:     
    ret
    
    
sol:
    mov bl,[b]
    mov ax,[c]
    neg ax
    idiv bl
    cbw
    ret 
  
root:
    mov al,[b]  
    cbw  
    mov dx,ax
    neg dx
    mov bh,[a]
    shl bh,1
    mov ax,dx
    add ax,cx    
    idiv bh
    cbw 
    push ax
    mov ax,dx
    sub ax,cx
    idiv bh
    cbw
    pop dx
    ret


square_root:
    cmp bx,0
    jne startsquare
    mov cx,0
    ret
startsquare:
    mov cx,1
    mov dx,1
loopsquare:     
    sub bx,dx
    cmp bx,0
    jle returnsquare
    add dx,2
    inc cx
    jmp loopsquare
    
returnsquare:
    cmp bx,0
    je finishsquare
    dec cx
finishsquare:
    ret    
     
discriminant:
    mov bl,[b]
    mov bh,[a]
    mov cx,[c]
    
    
    mov al,1
    imul bl
    imul bl
    push ax
    
    mov al,4
    imul bh
    imul cl
    
    pop bx
    sub bx,ax
ret
    
    
printNum:
    mov cx,0 
    mov dx,0
    mov si,0
    zero: 
        cmp ax,0
        jne negative 
        
        mov dx,48 
        mov ah,02h 
        int 21h     
        
        mov si,0
        jmp endprint 
    negative:
        cmp ax,0
        jg label1
        neg ax
        push ax
        mov dx,'-'
        mov ah,2
        int 21h
        mov si,1 
        pop ax  
        
        mov dx,0
                
  
    label1:  
        cmp ax,0 
        je finddif           
        mov bx,10                
        div bx                
        push dx              
        inc cx         
        xor dx,dx 
        jmp label1 
   
    finddif:     
        add si,cx  
    
    print1:                   
        cmp cx,0 
        je endprint              
        pop dx 
        add dx,48 
        mov ah,02h 
        int 21h             
        dec cx 
        jmp print1 
    endprint:
      
    ret     



printEnter: 
    mov dl, 10
    mov ah, 2
    int 21h
    mov dl, 13
    mov ah, 2
    int 21h
    ret        
    

      
ends

end start ; set entry point and stop the assembler.
