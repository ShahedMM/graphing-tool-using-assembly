# Graphing Tool

This project is a graphing tool that graphs equations in the form of ax^2 + b + c, where {a,b,c} are integers.

The tool is programmed using **Assembly language**.

The project is done as part of the Microprocessors course at Princess Sumaya University for Technology.
